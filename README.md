# yaml-templates

## Intro

This is a template repository that permit create others repositories in order to have a gitops repository.

## How to use

```
export APP_NAME="ruby-hello"
export NAMESPACE="demos"
export OWNER_NAME="Juan Enciso"
export TEAM_NAME="Devops"
export TEMPLATE="ruby-on-rails"

cp -pr ${TEMPLATE} ${APP_NAME}
for f in $(find ${APP_NAME} -name '*.yaml' -type f) ; do envsubst < $f > $f.tmp && mv $f.tmp $f ; done
```

This is a template repository that permit create create anothers repositories to use in gitops

